from pdb import pm
import numpy as np
import os
import pandas as pd
from datetime import datetime
import yaml
import glob

def parseYAML(path):
    with open(path, "r") as stream:
        try:
            picking_info = yaml.safe_load(stream)
            return picking_info
        except yaml.YAMLError as exc:
            print(exc)
    
def convertPickTimeToMilitary(pick_time):
    hr = pick_time[10:12]
    min = pick_time[13:15]
    sec = pick_time[16:18]
    am_pm = pick_time[29:]

    pick_time = hr + ':' + min + ':' + sec + ' ' + am_pm
    pick_time = getMilitaryTime(pick_time)
    return pick_time

def convertBagTimeFormat(bag_name):
    hr = bag_name[35:37]
    min = bag_name[38:40]
    sec = bag_name[41:43] 
    bag_time = hr + ':' + min + ':' + sec
    return bag_time

def getMilitaryTime(time):
    in_time = datetime.strptime(time, "%I:%M:%S %p")
    out_time = datetime.strftime(in_time, "%H:%M:%S")
    return out_time

def getTimeDifferenceSeconds(time1, time2):
    time_1 = datetime.strptime(time1, "%H:%M:%S")
    time_2 = datetime.strptime(time2, "%H:%M:%S")
    return (time_2 - time_1).total_seconds()

def getBagsWithPicks(bags_path, all_activity, item_master_path, output_dir):    
    bag_mapping = all_activity.copy()
    bag_mapping = bag_mapping.iloc[0:0]
    bag_mapping.insert(loc=0, column='bag_name', value = '')
    all_activity = all_activity.sort_values(by=["PICK_TIME"])
    pick_times = all_activity['PICK_TIME'].to_list()
    
    bags = pd.read_excel(bags_path, engine='xlrd')
    bags = bags.sort_values(by=["Bag Name"])
    
    for pick_time in pick_times:        
        conv_pick_time = convertPickTimeToMilitary(pick_time)
        
        for index, row in bags.iterrows():
            bag_time = convertBagTimeFormat(bags.iloc[index].item())
            next_bag_time = convertBagTimeFormat(bags.iloc[index+1].item())
            if conv_pick_time > bag_time and conv_pick_time < next_bag_time:
                if (getTimeDifferenceSeconds(bag_time, conv_pick_time) < 20):
                    temp_activity = all_activity.loc[all_activity['PICK_TIME'] == pick_time]
                    temp_activity.insert(loc=0, column='bag_name', value = bags.iloc[index-1].item())
                    temp_activity = temp_activity.append(all_activity.loc[all_activity['PICK_TIME'] == pick_time])
                    temp_activity.iloc[1, temp_activity.columns.get_loc('bag_name')] =  bags.iloc[index].item()                    
                else:
                    temp_activity = all_activity.loc[all_activity['PICK_TIME'] == pick_time]
                    temp_activity.insert(loc=0, column='bag_name', value = bags.iloc[index].item())
                bag_mapping = bag_mapping.append(temp_activity)
                break
            if index + 2 == bags.shape[0]:
                break

    bag_mapping = bag_mapping.drop_duplicates(subset=['bag_name', 'PICK_TIME'])
    bag_mapping = addItemMasterInfo(bag_mapping, item_master_path)    
    unique_bag_names = bag_mapping.drop_duplicates('bag_name')['bag_name']

    # drop unnecessary columns from unique_picks
    unique_picks = bag_mapping.drop_duplicates('PICK_TIME')
    unique_picks = unique_picks.drop('bag_name', 1)
    unique_picks = unique_picks.drop('SUBPACK_QTY', 1)
    unique_picks = unique_picks.drop('PACK_QTY', 1)
    unique_picks = unique_picks.drop('PALLET_QTY', 1)

    # Create directories
    os.chdir(output_dir[0:16])
    isExist = os.path.exists(output_dir[16:])
    if isExist == True:
        exit
    else:
        os.makedirs(output_dir[16:], 0o777)

    os.chdir('/home/vr-laptop-27/Documents/pickinbag')

    # to excel
    bag_mapping.to_excel(output_dir + '/bags_mapping.xlsx', header=True, index=False)
    unique_bag_names.to_excel(output_dir + '/bags_for_validators.xlsx', header=True, index=False)
    unique_picks.to_excel(output_dir + '/unique_picks.xlsx', header=True, index=False)

    print("Excel files created in: ", output_dir)

def addItemMasterInfo(bag_mapping, item_master_path):
    item_master = glob.glob(item_master_path + '*aster*.xlsx')
    item_master = pd.read_excel(item_master[0], engine='xlrd')
    bag_mapping["SUBPACK_QTY"] = ""
    bag_mapping["PACK_QTY"] = ""
    bag_mapping["PALLET_QTY"] = ""
    for index, row in bag_mapping.iterrows():
        if not item_master.loc[item_master['ITEM_NAME'] == row['ITEM_NAME']].empty:
            item_row = item_master.loc[item_master['ITEM_NAME'] == row['ITEM_NAME']]
            item_row = item_row.drop_duplicates('ITEM_NAME')
            if not pd.isnull(item_row['SUBPACK_QTY']).item():
                bag_mapping.at[index, 'SUBPACK_QTY'] = item_row['SUBPACK_QTY'].item()
            if not pd.isnull(item_row['PACK_QTY']).item():
                bag_mapping.at[index, 'PACK_QTY'] = item_row['PACK_QTY'].item()
            if not pd.isnull(item_row['PALLET_QTY']).item():
                bag_mapping.at[index, 'PALLET_QTY'] = item_row['PALLET_QTY'].item()
    return bag_mapping
        
def filterPicks(activity_tracking_path, pickerID, pick_type, code_desc):
    activity_tracking_list = glob.glob(activity_tracking_path + '*ctivity*.xlsx')
    all_activity = pd.DataFrame()
    for activity_tracking in activity_tracking_list:
        activity = pd.read_excel(activity_tracking, engine='xlrd')
        picker_activity = activity[activity['USER_ID'].isin(pickerID)]
        pallet_picking_activity = picker_activity[picker_activity['MENU_OPTN_NAME'].isin(pick_type)]
        pallet_picking_activity = pallet_picking_activity[pallet_picking_activity['CODE_DESC'].isin(code_desc)]
        pallet_picking_activity = pallet_picking_activity[pallet_picking_activity['NBR_UNITS'] != 0]
        pallet_picking_activity = pallet_picking_activity.drop_duplicates(subset=['PICK_TIME'])
        all_activity = all_activity.append(pallet_picking_activity)
    return all_activity

if __name__ == "__main__":
    picking_info = parseYAML('config.yaml')
    bags_path = picking_info['picking_info']['bag_path']
    pickerID = picking_info['picking_info']['picker_ID']
    pick_type = picking_info['picking_info']['pick_type']
    code_desc = picking_info['picking_info']['code_desc']
    output_dir = picking_info['picking_info']['output_dir']
    item_master_dir = output_dir[0:16]
    activity_tracking_path = item_master_dir
    pick_times = filterPicks(activity_tracking_path, pickerID, pick_type, code_desc)
    bags_with_picks = getBagsWithPicks(bags_path, pick_times, item_master_dir, output_dir)

